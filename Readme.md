# Description

Prototype of a 'general' Parser which aims to to 'correct' some misused notation in the madx files which break specifically the import in Xsuite when enabling the deferred expressions:
- arrow notation `->` for accessing the field of a variable 
- deferred expressions `:=` that assign directly to a number

in the first case a new variable is defined with the value of the field, in the second the columns are removed.


The main function (wrapper) to call is `Parser(filename)`, specifying the filename of the mad_file to correct, and a new file will be created under the name `final_corr_filename`.


## Attention
I know **very** little about madx, and this functions are easy but not generals nor efficients. For my cases they seem to work, and probably they can as well in many similar ones, but I tried just few cases; nonetheless they can possibly be made much more general. For that I need suggestions or eventually some help. 
Below are some limitations I am already aware of, so that you don't need to go through the code to spot the more problematic ones.




# Limitations/Assumptions

## `->` arrow related

### Searching for vars and fields around the -> sign
- Considered delimiters for recognizing variables and fields associated to each arrow use are
```
possible_enclosings = ['*', '/', ';', '+', '-', '^', ',', '(', ')',
                           '>']
```
- it doesn't consider if the arrow is in a comment (**#EasyFix**)

### searching for the parent_var defining the value of the field of the precedent one

- It considers as defining, only the first line that is structured like
```
var : 
```
and assumes the child_variable is the first word, followed the by the columns and then the parent_var, with the same delimiters as before, spaces are not a problem of course.

Highly problematic in lines with multiple definitions (**#Multiline #NotTooDifficultFix**).


### searching for the value of the parent_var

- same considerations as before in finding the definition of the parent_var

- No multiline considered, no definition in different line considered(**#Multiline #NotTooDifficultFix**)

- ~~takes the value, just searching for ':=' (**#stupid**, should be just '=')  in the definition line~~

-  ~~not checking if the parent_var field is the same as the child_var field (in my examples is always the same example L=L). I don't know if this is relevant (?)~~

- ~~not checking if parent_var defines more than one field, but naively assumes it's one~~ 

- ~~In general it assumes there's nothing to the right (example), alowing to take as right delimiter just ';'~~




### Applying the changes

- It goes by the easy-safe way of adding the new variables (named in the form of 'child_var'+'field' where child_var has '_' replacing each dot) at the beginning of the file. There is a simple describing comment

- The line to insert could be specified, but it's not safe against hitting-breaking other constructs (multiline-comment,macros def and every multiline command) **#EasyFix**




##  `:=` Related

Very simple version.

- It only deals wiht one deferred expression for each command line (ending with ';'). This to avoid looping through many delimiters since there could be many deferred expression in a file, and that could take much time (I think). But it is something to correct just for the specific cases #EasyFix

# Going to fix soon
- [X] stupid
- [ ] EasyFix

## Fixed
- in value seraching fixed the #stupid error of looking for := definitions of field
- In value searching, the function now can consider definition of different fields for the same parent_var, and in that case checks which one correspond to the field shared with the child_var





   


