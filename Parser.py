import numpy as np
import os

def give_all_ind(stringa ,char):
    count = 0
    ind_arr = []
    for i in stringa:
        if i == char:
            ind_arr.append(count)
        count +=1
    return ind_arr


def corr_col(filename, nfilename=None, verbose=True):
    with open(filename) as f:
        content = f.readlines()
    recons = []

    if verbose:
        print('Will print cases left to inspect visually :')
    for i in content:
        ilist = list(i)
        if ':=' in i:
            ind_comm = give_all_ind(i, '!')  # consider the comments
            # takes the := indexes first
            ind_col = [x for x in give_all_ind(i, ':') if i[x + 1] == '=']
            if len(ind_comm) != 0:
                ind_end = [x for x in give_all_ind(i, ';') if x < ind_comm[0]]
            else:
                ind_end = give_all_ind(i, ';')

            if len(ind_col) == len(ind_end):
                for x, y in zip(ind_col, ind_end):
                    if i[x + 1] == '=':
                        assigned = i[x + 2:y].strip()  #
                        try:

                            float(assigned)
                            ilist[x] = ''

                        except:

                            pass
            elif verbose:
                # CASES LEFT TO INSPECT VISUALLY
                print(i)

        recons.append(''.join(ilist))
    if nfilename is None:
        nfilename = 'final_' + filename
    with open(nfilename, 'w') as nf:
        for i in recons:
            nf.write(i)


def problem_finder(filename):

    with open(filename) as f:
        content = f.readlines()

    lines = []
    indexs = []
    for i in range(len(content)):
        if '->' in content[i]:
            lines.append(i)  # takes the lines' index where there's at least an arrow
            temp = []
            dash_ind = give_all_ind(content[i], '-')
            for ind in dash_ind:
                if content[i][ind + 1] == '>':
                    temp.append(ind)

            indexs.append(temp)  # for each line (incriminated) makes an array of index for the arrow

    ## LE DIMENSIONI DI lines e di indexs dovrebbero essere uguali

    possible_enclosings = ['*', '/', ';', '+', '-', '^', ',', '(', ')',
                           '>']
    if len(lines) == 0:
        print('No arrows, no problems')
        return None

    var_line = []
    field_line = []
    aggreg_var = []
    aggreg_field = []
    for ind, ind_row in enumerate(lines):
        # go throgh problematic lines
        var_left = []
        field_right = []
        lims_var = []
        lims_field = []
        for i in indexs[ind]:
            # go through all arrows in the line
            sym_ind = []
            # going through delimiters

            sx_nearest = -1
            dx_nearest = len(content[ind_row]) - 1

            for sym in possible_enclosings:

                temp = np.array(give_all_ind(content[ind_row], sym))

                if len(temp) != 0:
                    if len(temp[temp < i]) != 0:  # cerco tra i simboli alla SINISTRA
                        if np.max(temp[temp < i]) > (sx_nearest):
                            sx_nearest = np.max(temp[temp < i])

                    if len(temp[temp > i + 1]) != 0:  # cerco tra i simboli alla DESTRA
                        if np.min(temp[temp > i + 1]) < (dx_nearest):
                            dx_nearest = np.min(temp[temp > i + 1])
                    elif '!' in content[ind_row]:
                        print('seems that the rightmost symbol is !. Could be an error. Next is the addressed line: \n')
                        print(content[ind_row])
                        dx_nearest = content.find('!')

            lims_var.append((sx_nearest + 1, i))
            lims_field.append((i + 2, dx_nearest))
            left = content[ind_row][sx_nearest + 1:i].strip(' :;')
            right = content[ind_row][i + 2:dx_nearest].strip(' :;')

            var_left.append(left)
            field_right.append(right)

        var_line.append(var_left)
        field_line.append(field_right)

        aggreg_var.append((var_left, ind_row, lims_var))
        aggreg_field.append((field_right, ind_row, lims_field))


    return aggreg_var, aggreg_field


def search_var(word, file, max_line=None):
    # criterio cerca tra le linee dove c'è la parola seguita da due punti e
    # prendi quello che c'è tra i due punti e la parola
    # SEMPLICE ma sarebbe ottimale anche saltare direttamente le righe in cui c'è il richiamo,
    # solo che probabilmente saranno dopo e quiindi comunque non verrebbero messe al vaglio!
    with open(file) as f:
        content = f.readlines()
    if max_line is None:
        max_line = len(content)
    for n, i in enumerate(content):
        if n > max_line:
            print('superata linea limite')
            return None
        if word in i:

            col_indxs = give_all_ind(i, ':')
            if len(col_indxs) == 0:
                break
            if len(col_indxs) > 2:
                print('e mo chessefa?')
                return None
            else:
                coma_indxs = give_all_ind(i, ',')
                pointer = i[
                          col_indxs[0]:coma_indxs[0]]  # Assuming, it is the first thing in the line (Strong assumption)
                pointer = pointer.strip(' ;:')
                return pointer



def search_value(var,field, file):
    with open(file) as f:
        content = f.readlines()

    for n, i in enumerate(content):
        if var in i:
            col_ind = give_all_ind(i, ':')
            if var in i[:col_ind[0]]:
                eq_ind=give_all_ind(i,'=')
                if len(eq_ind)==1: # only one field defined here
                    ind = eq_ind[0]
                    coma_ind = np.array(give_all_ind(i, ',')) # only the comma is assumed as left delimiter
                    closest_coma = np.min(np.abs(coma_ind[coma_ind < ind] - ind))
                    #field = i[ind - closest_coma + 1:ind].strip(' :,') # kept in case we want to check the field is th correct one
                    delimiter = give_all_ind(i,';')
                    if len(delimiter)==1:
                        value = float(i[ind + 2:delimiter[0]])
                        del ind
                        return (value)
                    else:
                        print('Multiline not implemented for now')
                else :
                    for ind in eq_ind:
                        coma_ind = np.array(give_all_ind(i, ','))  # only the comma is assumed as left delimiter
                        coma_ind = coma_ind[coma_ind<ind] # consider just the left delimiter
                        closest_coma = np.min(np.abs(coma_ind[coma_ind < ind] - ind))
                        field_temp = i[ind - closest_coma + 1:ind].strip(' :,')
                        delimiters_poss = [',',';']
                        if field_temp==field:
                            dx_closest = len(i)
                            for x in delimiters_poss:
                                delim_ind  = np.array(give_all_ind(i,x))
                                delim_ind = delim_ind[delim_ind>ind]
                                if np.min(delim_ind-ind)<(dx_closest-ind):
                                    dx_closest= np.min(delim_ind-ind)+ind

                            value = float(i[ind + 2:dx_closest])
                            return (value)
                            


def name_chg(word):
    word_ = list(word)
    col_ind = give_all_ind(word, '.')
    for i in col_ind:
        word_[i] = '_'

    word_ = ''.join(word_)
    return word_


def medium_wrapper(finder_out, file, max_line=None, dict_out=None):
    vars_ = finder_out[0]
    field_line = finder_out[1]
    var_value_tocreate = []
    outdict = dict()
    for n, i in enumerate(vars_):
        # OGNI CICLO QUI CORRISPONDE AD UNA LINEA CON PIU' identificatori possibilmente
        # DA CONTROLLARE SE DIVENTA UN PROBLEMA CON 1 O DIVERSI NUMERI DI IDENTIFICATORI
        Vars = i[0]
        fields = field_line[n][0]

        for n_1, x in enumerate(Vars):
            field = fields[n_1]  # sarà utile solo per creare la variabile in modo significativo
            # No potrebbe essere un check con la funzione search value--> non sono sicuro ma potrebbe dove essere lo stesso
            parent_var = search_var(x, file, max_line)
            #
            parent_value = search_value2(parent_var,field, file)

            name = name_chg(x) + str(field)
            if dict_out is not None:
                outdict[x] = name
            var_value_tocreate.append((name, parent_value))

    if dict_out is not None:
        return var_value_tocreate, outdict
    else:
        return var_value_tocreate
        #########


def apply_changes(out_medium, file, n_line=0):
    '''
    Known problems that could break:
    - need to check where I write the things (because if inside a preesistent comment it can break it)

    '''
    with open(file) as f:
        content = f.readlines()
    newfilename = 'corr_arrow_' + file

    unique = set(out_medium)
    newlines = ['/' + '*' * 50 + '\n']
    newlines.append('*' + ' ' * 20 + 'DEFINITION OF THE NEW VARIABLES \n')
    newlines.append('*' * 50 + '/' + '\n')
    for x in unique:
        newlines.append(f'{x[0]} = {x[1]} ; \n')
    newlines.append('/' + '*' + ' ' * 20 + 'END of the DEFINITION' + '\n')
    newlines.append('*' * 50 + '/' + '\n')

    # print(newlines)
    # content.insert(n_line,newlines)
    content[n_line:n_line] = newlines
    # print(content[n_line+1])
    with open(newfilename, 'w') as nf:
        nf.write(''.join(content))


def chg_var(findout, file, outdict):
    with open(file) as f:
        content = f.readlines()

    Vars = findout[0]
    Fields = findout[1]
    for n_l, line in enumerate(Vars):
        line_list = list(content[line[1]])
        # print(''.join(line_list))
        chardiff = 0
        for n, vars_ in enumerate(line[0]):
            line_list[line[2][n][0] + chardiff:Fields[n_l][2][n][1] + chardiff] = outdict[vars_]
            chardiff = len(outdict[vars_]) - len(line_list[line[2][n][0]:Fields[n_l][2][n][1]])

        content[line[1]] = ''.join(line_list)

    filenew = 'chg_' + file
    with open(filenew, 'w') as fn:
        fn.write(''.join(content))

    return None


def parse(file):
    findout = problem_finder(file)
    var_val, outdict = medium_wrapper(findout, file, dict_out=True)
    chg_var(findout, file, outdict)  # this create the file named 'chg'+file
    apply_changes(var_val, 'chg_' + file)
    os.remove('chg_' + file)
    corr_col('corr_arrow_' + 'chg_' + file, nfilename='final_corr_' + file)
    os.remove('corr_arrow_' + 'chg_' + file)

    return None
